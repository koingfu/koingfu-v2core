# KoingFu V2Core

https://devwiki.koingfu.com

# **SmartBCH**

- network id: 10000
- Core/Factory address: 0xC0ab3bF9d799F7b020dCaB3A450ED141bfAe3A1e
- Router/Periphery address: 0xb457805fedfc2467877dCfA153636C1D22aE6c6F
- wBCH address: 0xc4eb62f900ae917f8F86366B4C1727eb526D1275
- RPC address: [https://smartbch.greyh.at](https://smartbch.greyh.at/)
- Multicall address: [https://www.smartscan.cash/address/0x36BD320fAB5b9d2CFd81FBcf6046F81B4F71A0BC](https://www.smartscan.cash/address/0x36BD320fAB5b9d2CFd81FBcf6046F81B4F71A0BC)


# **SmartBCH Amber Testnet**

- network id: 10001 (or hex 0x2711)
- Core/Factory address: 0xC0ab3bF9d799F7b020dCaB3A450ED141bfAe3A1e
- Router/Periphery address: 0xb457805fedfc2467877dCfA153636C1D22aE6c6F
- wBCH address: 0x21c7FD76B440e443aafb364f31396a3Ea0b3Abe0
- RPC address: [https://moeing.tech:9545/](https://moeing.tech:9545/)
- Multicall address: [https://www.smartscan.cash/address/0xd337ef88a3aE59D1130bB31D365de407D8a85e6a](https://www.smartscan.cash/address/0xd337ef88a3aE59D1130bB31D365de407D8a85e6a)



Binance Smart Chain
- Core / Factory contract address:  0x5873D455909cA064FBF3290DC34bf9fb46C16cdC
- https://bscscan.com/address/0x5873D455909cA064FBF3290DC34bf9fb46C16cdC#code
- Optimization enable, 999999, Istanbul, 0.5.16.
- Generated Fees are stored on: 0x3085B1D25bAD1ac083391cEfC3238AA5b1BD3628

AVAX
- Core / Factory contract address: 0x34362A81cdCe41b726B92AA1D6654205abB8f1B0
- https://snowtrace.io/address/0x34362A81cdCe41b726B92AA1D6654205abB8f1B0#code
- Optimization enable, 999999, Istanbul, 0.5.16.
- Generated Fees are stored on: 0x3d60Bb8D7772AdD9B06E619162FF55b68c4188c4

POLYGON
- Core / Factory contract address: 0x83B9b8B982fcb0f09d2f009ad12182d4d945F1E3
- https://polygonscan.com/address/0x83B9b8B982fcb0f09d2f009ad12182d4d945F1E3#code
- Optimization enable, 999999, Istanbul, 0.5.16.
- Generated Fees are stored on: 0xd3D23e22FaD904958C3aCfB71f2A9A9d3a373041

ROPSTEN TESTNET
- Core / Factory contract address: 0x2839C656e0Dc0884B33726d819A6af65BC3fb437
- https://ropsten.etherscan.io/address/0x2839C656e0Dc0884B33726d819A6af65BC3fb437
- Optimization enable, 999999, Istanbul, 0.5.16.
- Generated Fees are stored on: 0x878debC1Cf5f7c296bdE7368905C66A2de11Be4d

# Local Development

The following assumes the use of `node@>=10`.

## Install Dependencies

`yarn`

## Compile Contracts

`yarn compile`

## Run Tests

`yarn test`
